import { type ComponentPropsWithoutRef, type FormEvent } from 'react';

type FormProps = {
  onSave: (value: unknown) => void;
} & ComponentPropsWithoutRef<'form'>;

const Form: React.FC<FormProps> = (props) => {
  const { children, onSave, ...formProps } = props;

  const handleSubmit = (event: FormEvent<HTMLFormElement>) => {
    event.preventDefault();

    const formData = new FormData(event.currentTarget);
    const data = Object.fromEntries(formData);
    onSave(data);
  };

  return (
    <form onSubmit={handleSubmit} {...formProps}>
      {children}
    </form>
  );
};

export default Form;
