import { type ComponentPropsWithoutRef, forwardRef } from 'react';

type InputProps = {
  id: string;
  label: string;
} & ComponentPropsWithoutRef<'input'>;

/**
 * When I use forwardRef I can leave the React.FC<InputProps>
 */

const Input = forwardRef<HTMLInputElement, InputProps>((props, ref) => {
  const { id, label, ...inputProps } = props;

  return (
    <div>
      <label htmlFor={id}>{label}</label>
      <input id={id} name={id} {...inputProps} ref={ref} />
    </div>
  );
});

export default Input;
