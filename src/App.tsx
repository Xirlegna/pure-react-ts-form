import { useRef } from 'react';

import Input from './components/atoms/Input/Input';
import Form from './components/atoms/Form/Form';

type NameForm = {
  firstName: string;
  lastName: string;
};

const App = () => {
  const firstName = useRef<HTMLInputElement>(null);
  const lastName = useRef<HTMLInputElement>(null);

  const handleSave = (data: NameForm) => {
    console.log(data);
  };

  return (
    <main>
      <Form onSave={handleSave}>
        <Input id="firstName" label="First Name" type="text" ref={firstName} />
        <Input id="lastName" label="Last Name" type="text" ref={lastName} />
        <p>
          <button type="submit">Save</button>
        </p>
      </Form>
    </main>
  );
};

export default App;
